^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package costmap_cspace_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.3.1 (2019-01-09)
------------------

0.3.0 (2018-12-21)
------------------
* Fix package dependencies (`#5 <https://github.com/at-wat/neonavigation_msgs/issues/5>`_)

  * Fix package dependencies
  * Migrate to manifest format 2
  * Add catkin_lint test

* Contributors: Atsushi Watanabe

0.2.0 (2018-06-20)
------------------
* Fix package dependencies (`#2 <https://github.com/at-wat/neonavigation_msgs/issues/2>`_)
* Fix package versions (`#1 <https://github.com/at-wat/neonavigation_msgs/issues/1>`_)
* Initial drop from neonavigation meta-package
* Contributors: Atsushi Watanabe
