^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package trajectory_tracker_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.3.1 (2019-01-09)
------------------
* trajectory_tracker_msgs: fix header install directory (`#13 <https://github.com/at-wat/neonavigation_msgs/issues/13>`_)
* Contributors: Atsushi Watanabe

0.3.0 (2018-12-21)
------------------
* trajectory_tracker_msgs: add PathWithVelocity message (`#9 <https://github.com/at-wat/neonavigation_msgs/issues/9>`_)

  * Add PathWithVelocity
  * Add message converter from Path to PathWithVelocity
  * Add comment of linear_velocity nan value
  * Enable C++11 on test

* Fix package dependencies (`#5 <https://github.com/at-wat/neonavigation_msgs/issues/5>`_)

  * Fix package dependencies
  * Migrate to manifest format 2
  * Add catkin_lint test

* Contributors: Atsushi Watanabe

0.2.0 (2018-06-20)
------------------
* Fix package dependencies (`#2 <https://github.com/at-wat/neonavigation_msgs/issues/2>`_)
* Initial drop from neonavigation meta-package
* Contributors: Atsushi Watanabe
