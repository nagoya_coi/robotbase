ROS robot base

# clone repo
git clone git@bitbucket.org:nagoya_coi/robotbase.git

# install yp_spur
follow instructions at https://github.com/openspur/yp-spur
1. git clone yp-spur
2. install
```
$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install
```

# install dependencies
you can replace kinetic with the ros distro you are using, but this has been developed using kinetic
```
rosdep install --from-paths src --ignore-src --rosdistro=kinetic -y
```

# compile
Run the custom compile script instead of just catkin make directly, as we need to set some environment variables
```
./catkin_make_release
```

# running robot launch script
Make sure to source in each new tab you run ros scripts from
```
# in one tab:
source ./devel/setup.bash
roscore

# in a second tab:
source ./devel/setup.bash
roslaunch neonavigation_launch demo_joystick.launch
```

# driving the robot
```
# in one tab:
source ./devel/setup.bash
roscore

# connect robot
# setup connections
sudo ldconfig
cd scripts/
chmod +x write_udev_rule
sudo ./write_udev_rule
cd ..
roslaunch launch/drive.launch
```
